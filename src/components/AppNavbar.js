import {useContext} from "react";
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from "../UserContext";


export default function AppNavbar(){

	const { user } = useContext(UserContext)

	return(

		<Navbar bg="light" expand="lg">
			<Container fluid>
				{
					(user.isAdmin)?
						<Navbar.Brand as={NavLink} to="/admin">Britino</Navbar.Brand>
						:
						<Navbar.Brand as={NavLink} to="/menu">Britino</Navbar.Brand>
				}
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						{
							((user.id))?
								((user.isAdmin))?
								<>
									<Nav.Link as={NavLink} to="/admin">Dashboard</Nav.Link>
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>
								:
								<>
									<Nav.Link as={NavLink} to="/menu">Menu</Nav.Link>
									<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
								</>
							:
							<>
								<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
							</>
						}

					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}