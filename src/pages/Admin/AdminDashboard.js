import Menu from './AdminMenuView'
import {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';

export default function AdminDashboard(){
    const [products, setProducts] = useState([]);
    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/products`,{
          headers : {
            "Authorization" : `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
            setProducts(data.map((product=>{
            return(
                <Menu key={product._id} product={product}/>
        )
    })
            ))
        })
    }, [])

    return(
        <>  
           <div className="text-center">
                <h2 className="text-center">Admin Dashboard</h2>
                <Link className="btn btn-primary btn-block" to = "/admin/addProduct">Add a new product</Link>
           </div>
            {products}
        </>
    )
}